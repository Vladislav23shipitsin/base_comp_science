package firstTask.statement;

import firstTask.Student;
import firstTask.exceptions.StatementException;
import firstTask.statement.Rating;
import firstTask.statement.Statement;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TestStatement {

    private String discipline = "Base comp science";
    private String nameLecture = "Ashaev I.V.";
    private int semestr = 8;
    private String year = "2019/2020";
    private Set<String> groups = new HashSet<>();
    private  Date date = new Date();



    private Student student1 = new Student("Иван","IMIT","MMS");
    private Student student2 = new Student("Петр","FKN","LOL");
    private Student student3 = new Student("Мария","IMIT","MMS");
    private Student student4 = new Student("Илья","FilFak","FFK");



    public Statement createStatement(){
        return new Statement(discipline,nameLecture,date,semestr,year,groups);
    }

    @Test
    public void testGet(){
        Statement statement = createStatement();

        assertEquals(discipline,statement.getDiscipline());
        assertEquals(nameLecture,statement.getNameLecturer());
        assertEquals(semestr,statement.getSemester());
        assertEquals(date,statement.getDate());
        assertEquals(year,statement.getYear());

        Set<String> getGroups = statement.getGroups();
        Map<Student, Rating> records = statement.getRecords();

        assertNotNull(getGroups);
        assertNotNull(records);
        assertTrue(getGroups.isEmpty());
        assertTrue(records.isEmpty());

    }

    @Test
    public void testShow() throws StatementException {
        Statement statement = createStatement();
        statement.addRecord(student1,Rating.COOL);
        statement.addRecord(student2,Rating.FAIL);
        statement.addRecord(student3,Rating.ABSENCE);
        System.out.println(statement);
    }

    @Test
    public void testAddCorectWithoutGroups() throws StatementException {
        Statement statement = createStatement();

        statement.addRecord(student1,Rating.COOL);
        statement.addRecord(student2,Rating.FAIL);
        statement.addRecord(student3,Rating.ABSENCE);

        testCheckRecords(statement);

    }

    @Test
    public void testAddCorrectWithGroups() throws StatementException {
        Statement statement = createStatement();
        Set<String> groups = new HashSet<>();
        groups.add("MMS");
        groups.add("LOL");
        groups.add("AAA");
        statement.setGroups(groups);

        statement.addRecord(student1,Rating.COOL);
        statement.addRecord(student2,Rating.FAIL);
        statement.addRecord(student3,Rating.ABSENCE);

        testCheckRecords(statement);

    }

    @Test(expected = StatementException.class)
    public void testAddWrong() throws StatementException {
        Statement statement = createStatement();
        Set<String> groups = new HashSet<>();
        groups.add("MMS");
        groups.add("LOL");
        groups.add("AAA");
        statement.setGroups(groups);

        statement.addRecord(student1,Rating.COOL);
        statement.addRecord(student2,Rating.FAIL);
        statement.addRecord(student3,Rating.ABSENCE);
        statement.addRecord(student4,Rating.NOT);

        testCheckRecords(statement);
    }

    public void testCheckRecords(Statement statement){
        Map<Student,Rating> resultMap = statement.getRecords();
        assertEquals(3,resultMap.size());

        assertTrue(resultMap.containsKey(student1));
        assertTrue(resultMap.containsKey(student2));
        assertTrue(resultMap.containsKey(student3));

        assertEquals(Rating.COOL,resultMap.get(student1));
        assertEquals(Rating.FAIL,resultMap.get(student2));
        assertEquals(Rating.ABSENCE,resultMap.get(student3));
    }

    @Test
    public void testDelete() throws StatementException {
        Statement statement = createStatement();

        statement.addRecord(student1,Rating.COOL);
        statement.addRecord(student2,Rating.FAIL);
        statement.addRecord(student3,Rating.ABSENCE);
        statement.addRecord(student4,Rating.ABSENCE);

        assertEquals(4,statement.getRecords().size());

        statement.deleteRecord(student4);

        testCheckRecords(statement);
    }

    @Test
    public void testChangeCorrect() throws StatementException {
        Statement statement = createStatement();

        statement.addRecord(student1,Rating.COOL);
        statement.addRecord(student2,Rating.NOT);
        statement.addRecord(student3,Rating.ABSENCE);

        statement.changeRating(student2,Rating.FAIL);

        testCheckRecords(statement);

    }

    @Test(expected = StatementException.class)
    public void testChangeWrong() throws StatementException {
        Statement statement = createStatement();

        statement.addRecord(student1,Rating.COOL);
        statement.addRecord(student2,Rating.NOT);
        statement.addRecord(student3,Rating.ABSENCE);

        statement.changeRating(student4,Rating.FAIL);
    }

    @Test
    public void addStudentTwo() throws StatementException {
        Statement statement = createStatement();

        statement.addRecord(student1,Rating.COOL);
        statement.addRecord(student1,Rating.FAIL);

        assertEquals(1,statement.getRecords().size());
    }

    @Test
    public void testCorrectGetRecords() throws StatementException {
        Statement statement = createStatement();

        statement.addRecord(student1,Rating.COOL);

        Map<Student,Rating> resultMap = statement.getRecords();

        resultMap.remove(student1);

        assertEquals(0,resultMap.size());
        assertEquals(1,statement.getRecords().size());

        Student resultStudent = statement.getRecords().keySet().iterator().next();

        assertEquals(resultStudent,student1);

        resultStudent.setName("Another");

        assertEquals("Another",resultStudent.getName());
        assertEquals("Иван",student1.getName());

    }

    @Test
    public void testCheckAlphabetOrder() throws StatementException {
        Statement statement = createStatement();

        statement.addRecord(student1,Rating.COOL);
        statement.addRecord(student2,Rating.COOL);
        statement.addRecord(student3,Rating.COOL);
        statement.addRecord(student4,Rating.COOL);

        Map<Student,Rating> resultRecords = statement.getRecords();

        Iterator<Student> iter = resultRecords.keySet().iterator();
        assertEquals("Иван",iter.next().getName());
        assertEquals("Илья",iter.next().getName());
        assertEquals("Мария",iter.next().getName());
        assertEquals("Петр",iter.next().getName());

    }
}
