package firstTask.statement;

import firstTask.Student;
import firstTask.exceptions.StatementException;
import firstTask.statement.Rating;
import firstTask.statement.Statement;
import firstTask.statement.SummaryStatement;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestSummaryStatement {

    private SummaryStatement summaryStatementMMS;
    private SummaryStatement summaryStatementMPB;

    @Before
    public void beforeTest(){
        List<SummaryStatement> summaryStatements = createSummaryStatement();
        summaryStatementMMS = summaryStatements.get(0);
        summaryStatementMPB = summaryStatements.get(1);
    }

    private static final String YEAR = "2019/2020";
    private static final int SEMESTR = 8;
    private static final String MMS = "ММС-601-0";
    private static final String MPB = "МПБ-601-0";

    private static final String FACULTY = "ИМИТ";
    private static Student student11 = new Student("Адельшин",FACULTY,MMS);
    private static  Student student12 = new Student("Таймре",FACULTY,MMS);
    private static  Student student13 = new Student("Садуллаева",FACULTY,MMS);
    private static  Student student14 = new Student("Шипицин",FACULTY,MMS);

    private Student student21 = new Student("Козлова",FACULTY,MPB);
    private static  Student student22 = new Student("Беркутов",FACULTY,MPB);
    private static  Student student23 = new Student("Ковалев",FACULTY,MPB);
    private static  Student student24 = new Student("Козуев",FACULTY,MPB);
    private static  Student student25 = new Student("Ющенко",FACULTY,MPB);

    private static  String subjectBaseCompScience = "Основы компьютерных наук";
    private static  String subjectMathAnalZachet= "Математический анализ зачет";
    private static  String subjectMathAnalExam = "Математический анализ экзамен";
    private static  String subjectPrograming = "Программирование";

    private static  String ASHAEV = "Ашаев Игорь Викторович";
    private static  String MELNIKOV = "Мельников Евгений Владимирович";

    private static Statement statementBaseCopmScience =
            new Statement(subjectBaseCompScience,ASHAEV,new Date(2019,9,18),SEMESTR,YEAR,MMS);

    private static Statement statementPrograming =
            new Statement(subjectPrograming,ASHAEV,new Date(2019,9,18),SEMESTR,YEAR,MPB);

    private static Statement statementMathAnalZachet1 =
            new Statement(subjectMathAnalZachet,MELNIKOV,new Date(2019,9,19),SEMESTR,YEAR);

    private static Statement statementMathAnalZachet2 =
            new Statement(subjectMathAnalZachet,MELNIKOV,new Date(2019,10,1),SEMESTR,YEAR);

    private static Statement statementMathAnalExam1 =
            new Statement(subjectMathAnalExam,MELNIKOV,new Date(2019,9,20),SEMESTR,YEAR);

    private static Statement statementMathAnalExam2 =
            new Statement(subjectMathAnalExam,MELNIKOV,new Date(2019,10,10),SEMESTR,YEAR);

    private static Statement statementMathAnalExam3 =
            new Statement(subjectMathAnalExam,MELNIKOV,new Date(2019,10,15),SEMESTR,YEAR);


    public List<SummaryStatement> createSummaryStatement(){
        List<SummaryStatement> summaryStatements = new ArrayList<>();
        try {
            statementBaseCopmScience.addRecord(student11, Rating.COOL);
            statementBaseCopmScience.addRecord(student12,Rating.GOOD);
            statementBaseCopmScience.addRecord(student13,Rating.COOL);
            statementBaseCopmScience.addRecord(student14,Rating.GOOD);

            statementPrograming.addRecord(student21,Rating.COOL);
            statementPrograming.addRecord(student22,Rating.COOL);
            statementPrograming.addRecord(student23,Rating.COOL);
            statementPrograming.addRecord(student24,Rating.GOOD);
            statementPrograming.addRecord(student25,Rating.MEDIUM);

            statementMathAnalZachet1.addRecord(student11,Rating.PASS);
            statementMathAnalZachet1.addRecord(student12,Rating.PASS);
            statementMathAnalZachet1.addRecord(student13,Rating.PASS);
            statementMathAnalZachet1.addRecord(student14,Rating.ABSENCE);
            statementMathAnalZachet1.addRecord(student21,Rating.PASS);
            statementMathAnalZachet1.addRecord(student22,Rating.PASS);
            statementMathAnalZachet1.addRecord(student23,Rating.PASS);
            statementMathAnalZachet1.addRecord(student24,Rating.FAIL);
            statementMathAnalZachet1.addRecord(student25,Rating.PASS);

            statementMathAnalExam1.addRecord(student11,Rating.COOL);
            statementMathAnalExam1.addRecord(student12,Rating.COOL);
            statementMathAnalExam1.addRecord(student13,Rating.COOL);
            statementMathAnalExam1.addRecord(student14,Rating.ABSENCE);
            statementMathAnalExam1.addRecord(student21,Rating.MEDIUM);
            statementMathAnalExam1.addRecord(student22,Rating.COOL);
            statementMathAnalExam1.addRecord(student23,Rating.GOOD);
            statementMathAnalExam1.addRecord(student24,Rating.BAD);
            statementMathAnalExam1.addRecord(student25,Rating.GOOD);

            statementMathAnalZachet2.addRecord(student14,Rating.PASS);
            statementMathAnalZachet2.addRecord(student24,Rating.PASS);

            statementMathAnalExam2.addRecord(student24,Rating.BAD);
            statementMathAnalExam2.addRecord(student14,Rating.GOOD);

            statementMathAnalExam3.addRecord(student24,Rating.MEDIUM);

            TreeSet<Statement> statements = new TreeSet<>();
            statements.add(statementBaseCopmScience);
            statements.add(statementPrograming);
            statements.add(statementMathAnalExam1);
            statements.add(statementMathAnalExam2);
            statements.add(statementMathAnalExam3);
            statements.add(statementMathAnalZachet1);
            statements.add(statementMathAnalZachet2);

            SummaryStatement summaryStatementMMS =
                    new SummaryStatement(YEAR,SEMESTR,MMS,statements);

            summaryStatementMMS.build();

            SummaryStatement summaryStatementMPB =
                    new SummaryStatement(YEAR,SEMESTR,MPB,statements);

            summaryStatementMPB.build();

            summaryStatements.add(summaryStatementMMS);
            summaryStatements.add(summaryStatementMPB);

        } catch (StatementException e) {
            e.printStackTrace();
        }
        return summaryStatements;
    }

    @Test
    public void testShow(){
        System.out.println(summaryStatementMMS);
        System.out.println(summaryStatementMPB);
    }

    @Test
    public void testGetRating(){
        List<Rating> ratingList = summaryStatementMPB.getRatings(student24,subjectMathAnalExam);
        assertEquals(3,ratingList.size());
        assertEquals(Rating.BAD,ratingList.get(0));
        assertEquals(Rating.BAD,ratingList.get(1));
        assertEquals(Rating.MEDIUM,ratingList.get(2));
    }

    @Test
    public void testAddStatement(){
        String subject = "Физкультура";
        String nameLecture = "Физрук";
        Statement newStatement = new Statement(subject,nameLecture,new Date(2019,1,11),SEMESTR,YEAR);
        try {
            newStatement.addRecord(student12,Rating.COOL);
            newStatement.addRecord(student13,Rating.COOL);
            summaryStatementMMS.addStatement(newStatement);
            System.out.println(summaryStatementMMS);
        } catch (StatementException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testGetSubjects(){
        Set<String> subjectSet = summaryStatementMMS.getSubejcts();
        assertEquals(3,subjectSet.size());
        assertTrue(subjectSet.contains(subjectBaseCompScience));
        assertTrue(subjectSet.contains(subjectMathAnalExam));
        assertTrue(subjectSet.contains(subjectMathAnalZachet));
    }

    @Test
    public void testGetStudenst(){
        Set<Student> students = summaryStatementMMS.getStudents();
        assertEquals(4,students.size());
        assertTrue(students.contains(student11));
        assertTrue(students.contains(student12));
        assertTrue(students.contains(student13));
        assertTrue(students.contains(student14));
    }

    @Test
    public void testBuild(){
        System.out.println(summaryStatementMMS);
        summaryStatementMMS.build();
        System.out.println(summaryStatementMMS);
    }


}
