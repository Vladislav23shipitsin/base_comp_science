package firstTask;

import firstTask.Student;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestStudent {

    private String name = "Иванов Иван Иванович";
    private String faculty = " IMIT";
    private String group = " MMS-601-0";

    public Student createStudent(){
        return new Student(name,faculty,group);
    }

    @Test
    public void testCreate(){
        Student student = createStudent();

        assertEquals(name,student.getName());
        assertEquals(faculty,student.getFaculty());
        assertEquals(group,student.getGroup());
        assertNotNull(student.getId());
    }

    @Test
    public void testSet(){
        Student student = createStudent();
        student.setName("Petr");
        student.setFaculty("FKN");
        student.setGroup("LOL");

        assertEquals("Petr",student.getName());
        assertEquals("FKN",student.getFaculty());
        assertEquals("LOL",student.getGroup());
        assertNotNull(student.getId());
    }

    @Test
    public void testShow(){
        Student student = createStudent();
        System.out.println(student);
    }

    @Test
    public void testToCompare(){
        Student student1 = new Student("Alex","IMIT","MMS");
        Student student2 = new Student("Bad","IMIT","MMS");
        Student student3 = new Student("Alex","FKN","MML");

        assertTrue(student1.compareTo(student2) < 0);
        assertTrue(student2.compareTo(student1) > 0);
        assertEquals(0, student2.compareTo(student2));
        assertEquals(0, student1.compareTo(student3));

    }


}
