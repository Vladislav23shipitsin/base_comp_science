package firstTask;

import java.util.Objects;
import java.util.UUID;

public class Student implements Comparable<Student> {

    private UUID id;
    private String group;
    private String name;
    private String faculty;

    public Student(String name, String faculty, String group) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.faculty = faculty;
        this.group = group;
    }

    public Student(Student student){

        // TODO берем студента меняем ему имя, получаем 2 студента с 1 id но разными именами
        // TODO условие равенства студентов ?

        this(student.getName(),student.getFaculty(),student.getGroup());
        this.id = student.getId();
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    @Override
    public String toString() {
        StringBuffer s = new StringBuffer();
        s.append(getName());
        s.append(",");
        s.append(getFaculty());
        s.append(",");
        s.append(getGroup());
        return s.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return Objects.equals(getId(), student.getId()) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public int compareTo(Student student) {
        return getName().compareTo(student.getName());
    }
}
