package firstTask.statement;

import firstTask.Student;
import firstTask.exceptions.EnumStatementException;
import firstTask.exceptions.StatementException;

import java.util.*;

public class Statement implements Comparable<Statement>{
    private String discipline;
    private String nameLecturer;
    private Date date;
    private int semester;
    private String year;
    private Set<String> groups;
    private Map<Student, Rating> ratingMap;


    public Statement(String discipline, String nameLecturer, Date date, int semester, String year) {
        this.discipline = discipline;
        this.nameLecturer = nameLecturer;
        this.date = date;
        this.semester = semester;
        this.year = year;
        groups = new HashSet<>();
        ratingMap = new TreeMap<>();
    }
    public Statement(String discipline, String nameLecturer, Date date, int semester, String year,String group) {
        this(discipline,nameLecturer,date,semester,year);
        groups.add(group);
    }
    public Statement(String discipline, String nameLecturer, Date date, int semester, String year,Set<String> groups) {
        this(discipline,nameLecturer,date,semester,year);
        setGroups(groups);
    }

    public String getDiscipline() {
        return discipline;
    }
    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }
    public String getNameLecturer() {
        return nameLecturer;
    }
    public void setNameLecturer(String nameLecturer) {
        this.nameLecturer = nameLecturer;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public int getSemester() {
        return semester;
    }
    public void setSemester(int semester) {
        this.semester = semester;
    }
    public String getYear() {
        return year;
    }
    public void setYear(String year) {
        this.year = year;
    }
    public Set<String> getGroups() {
        return groups;
    }
    public void setGroups(Set<String> groups) {
        this.groups.addAll(groups);
    }

    public Map<Student,Rating> getRecords(){
        Map<Student,Rating> resultMap = new TreeMap<>();
        for(Student student : ratingMap.keySet()){
            resultMap.put(new Student(student),ratingMap.get(student));
        }
        return resultMap;
    }

    public void addRecord(Student student, Rating rating) throws StatementException {
        if(!groups.isEmpty() && !groups.contains(student.getGroup()))
            throw new StatementException(EnumStatementException.WRONG_GROUP);
        ratingMap.put(student,rating);
    }

    public void deleteRecord(Student student){
        ratingMap.remove(student);
    }

    public void changeRating(Student student,Rating newRating) throws StatementException {
        if(!ratingMap.containsKey(student)) throw new StatementException(EnumStatementException.NO_SUCH_STUDENT);
        ratingMap.put(student,newRating);
    }

    @Override
    public String toString() {
      StringBuffer s = new StringBuffer();
      s.append("Ведомость по дисциплине: ");
      s.append(discipline);
      s.append("\n");
      s.append("Дата: ");
      s.append(date);
      s.append("  Семестр: ");
      s.append(semester);
      s.append("  Учебный год:");
      s.append(year);
      s.append("  Преподаватель: ");
      s.append(nameLecturer);
      s.append("\n");
      s.append("---------------------------------------------------------------------");
      for(Student student : ratingMap.keySet()){
          s.append("\n");
          s.append(student);
          s.append("-");
          s.append(ratingMap.get(student));
      }
      s.append("\n---------------------------------------------------------------------");
      return s.toString();
    }

    @Override
    public int compareTo(Statement o) {
        int result = getDiscipline().compareTo(o.getDiscipline());
        if (result == 0){
            return getDate().compareTo(o.getDate());
        }else{
            return result;
        }
    }
}
