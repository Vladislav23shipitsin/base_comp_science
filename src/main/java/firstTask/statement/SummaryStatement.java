package firstTask.statement;

import firstTask.Student;

import java.util.*;

public class SummaryStatement {
    private Set<Statement> statements;
    private String year;
    private int semestr;
    private String group;
    private Map<Student,Row> table;
    private Set<String> subejcts;

    public SummaryStatement(String year, int semestr, String group, TreeSet<Statement> statements) {
        this.statements = statements;
        this.year = year;
        this.semestr = semestr;
        this.group = group;
        table = new TreeMap<>();
        subejcts = new TreeSet<>();
    }
    public Set<Statement> getStatements() {
        return statements;
    }


    public void addStatement(Statement statement){
        if(isCorrectStatement(statement)){
            statements.add(statement);
            this.build();
        }
    }

   /* public void deleteStatement(Statement statement){
        statements.remove(statement);
        this.build();
    }*/
    public void setStatements(Set<Statement> statements) {
        this.statements = statements;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getSemestr() {
        return semestr;
    }

    public void setSemestr(int semestr) {
        this.semestr = semestr;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public boolean isCorrectStatement(Statement statement){
        return statement.getSemester() == getSemestr()
                && statement.getYear().equals(getYear())
                &&
                (statement.getGroups().contains(getGroup()) || statement.getGroups().isEmpty());
    }
    public boolean isCorrectStudent(Student student){
        return student.getGroup().equals(getGroup());
    }

    public List<Rating> getRatings(Student student,String subject){
        return table.get(student).getRatings(subject);
    }

    public Set<String> getSubejcts(){
        return new TreeSet<>(subejcts) ;
    }
    public Set<Student> getStudents(){
        return new TreeSet<>(table.keySet());
    }

    public void build(){
        for(Statement statement : statements){
            if(isCorrectStatement(statement)){
                subejcts.add(statement.getDiscipline());
                for(Student student : statement.getRecords().keySet()){
                    if(isCorrectStudent(student)){
                        if(!table.containsKey(student)){
                            table.put(student,new Row());
                        }
                        table.get(student)
                                .add(statement.getDiscipline(),statement.getDate(),statement.getRecords().get(student)
                                );
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuffer s = new StringBuffer();
        s.append("Сводная ведомость группы:");
        s.append(group);
        s.append("  За учебный год:");
        s.append(year);
        s.append("  Семестр:");
        s.append(semestr);
        s.append("\n");
        s.append("------------------------------------------------------");
        s.append("\n");
        s.append("| № | ФИО | ");
        for(String subject : subejcts){
            s.append(subject);
            s.append(" | ");
        }
        int count = 0;

        for(Student student : table.keySet()){
            s.append("\n");
            s.append(" | ");
            s.append(++count);
            s.append(" | ");
            s.append(student.getName());
            s.append(table.get(student).toString());
            s.append(" | ");
        }
        s.append("\n");
        s.append("------------------------------------------------------");
        return s.toString();
    }
}
