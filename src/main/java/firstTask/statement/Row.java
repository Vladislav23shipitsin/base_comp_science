package firstTask.statement;

import java.util.*;

public class Row {

    // дата - > оценка
    private class DataRating implements Comparable<DataRating>{
        private Date date;
        private Rating rating;

        public DataRating(Date date, Rating rating) {
            this.date = date;
            this.rating = rating;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public Rating getRating() {
            return rating;
        }

        public void setRating(Rating rating) {
            this.rating = rating;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof DataRating)) return false;
            DataRating that = (DataRating) o;
            return Objects.equals(getDate(), that.getDate()) &&
                    getRating() == that.getRating();
        }

        @Override
        public int hashCode() {
            return Objects.hash(getDate(), getRating());
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer();
            sb.append(date.getDate());
            sb.append(":");
            sb.append(date.getMonth());
            sb.append(" - ");
            sb.append(rating);
            return sb.toString();
        }

        @Override
        public int compareTo(DataRating o) {
            return date.compareTo(o.getDate());
        }
    }

    // отображает дисциплину в оценки
    private Map<String,Set<DataRating>> cells;

    public Row(){
        cells = new TreeMap<>();
    }

    public void add(String subject,Date date,Rating rating){
        if(!cells.containsKey(subject)){
            cells.put(subject,new TreeSet<>());
        }
        cells.get(subject).add(new DataRating(date,rating));
    }
    public List<Rating> getRatings(String subject){
        List<Rating> ratings = new ArrayList<>();
        for(DataRating dataRating : cells.get(subject)){
            ratings.add(dataRating.getRating());
        }
        return ratings;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        for(String s : cells.keySet()) {
            sb.append(" | ");
            for (DataRating dataRating : cells.get(s)) {
                sb.append(dataRating);
                sb.append(";");
            }
        }
        return sb.toString();
    }
}
