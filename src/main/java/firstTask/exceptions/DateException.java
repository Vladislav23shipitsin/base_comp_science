package firstTask.exceptions;

public class DateException extends Exception {
    private EnumDateExeption error;
    public DateException(EnumDateExeption error) {
        this.error = error;
    }

    @Override
    public String getMessage() {
        return error.toString();
    }

    public DateException(String message) {
        super(message);
    }

    public DateException(String message, Throwable cause) {
        super(message, cause);
    }

    public DateException(Throwable cause) {
        super(cause);
    }
}
