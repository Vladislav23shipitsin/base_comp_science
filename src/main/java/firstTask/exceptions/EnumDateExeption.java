package firstTask.exceptions;

public enum EnumDateExeption {
    WRONG_DAY("Такого дня нет"),WRONG_MONTH("Такого месяца нет"),WRONG_YEAR("Такого года нет");
    private String message;
    EnumDateExeption(String message){
        this.message = message;
    }

    @Override
    public String toString() {
        return  message ;
    }
}
