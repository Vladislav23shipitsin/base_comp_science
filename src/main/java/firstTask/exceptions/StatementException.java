package firstTask.exceptions;

public class StatementException extends Exception {
    private EnumStatementException error;
    public StatementException(EnumStatementException error) {
        this.error = error;
    }

    @Override
    public String getMessage() {
        return error.toString();
    }

    public StatementException(String message) {
        super(message);
    }

    public StatementException(String message, Throwable cause) {
        super(message, cause);
    }

    public StatementException(Throwable cause) {
        super(cause);
    }
}
