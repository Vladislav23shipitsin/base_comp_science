package firstTask.exceptions;

public enum EnumStatementException {
    WRONG_GROUP("Группа студента не совпадает с группами в ведомости"),
    NO_SUCH_STUDENT("Студента нет в ведомости");
    private String message;
    EnumStatementException(String message){
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
