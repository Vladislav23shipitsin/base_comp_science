package firstTask;

import firstTask.statement.Rating;
import firstTask.statement.SummaryStatement;

import java.util.*;

public class WorkStatement {

    public static Map<Student, Set<String>> getDebtors(SummaryStatement summaryStatement){
        Map<Student, Set<String>> debtors = new TreeMap<>();
        for(Student student : summaryStatement.getStudents()){
            for(String subject : summaryStatement.getSubejcts()){

                List<Rating> ratings = summaryStatement.getRatings(student,subject);
                int size = ratings.size();
                Rating rating = ratings.get(size - 1);
                if(rating == Rating.ABSENCE || rating == Rating.BAD || rating == Rating.FAIL){
                    if(!debtors.containsKey(student)){
                        debtors.put(student,new HashSet<>());
                    }
                    debtors.get(student).add(subject);
                }
            }
        }

        return debtors;
    }

}
